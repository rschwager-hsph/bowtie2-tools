import os
import sys
import operator
import subprocess
import threading
import tempfile
from contextlib import contextmanager
from functools import partial
from itertools import dropwhile, ifilter, imap

try:
    import bz2
    import gzip
except:
    pass

from toolz import curry, count, compose, countby, join
from toolz.curried import get
from Bio import SeqIO, Seq, SeqRecord
import pysam


min_len = 75
min_qual = 20
min_mapping_score = 20

lt = curry(operator.gt) #these are switched because of how curry() works
gt = curry(operator.lt) #these are switched because of how curry() works
eq = curry(operator.eq)
add = curry(operator.add)
low_quality = lt(min_qual)
good_mapping = compose(gt(min_mapping_score), int, get(4)) 

def my_open(f, *args, **kwargs):
    if f == '-':
        return sys.stdin
    elif f.endswith(".bz2"):
        return bz2.BZ2File(f, *args, **kwargs)
    elif f.endswith("gzip") or f.endswith("gz"):
        return gzip.GzipFile(f, *args, **kwargs)
    else:
        return open(f, *args, **kwargs)


def handle_samfile(file_str, filemode="r"):
    sam_file = pysam.Samfile(file_str, filemode, 
                             check_header=False, check_sq=False)
    for read in sam_file:
        seq = Seq.Seq(read.seq)
        qual = [ ord(x)-33 for x in read.qual ]
        if read.is_reverse:
            seq = seq.reverse_complement()
        yield SeqRecord.SeqRecord(
            seq, read.qname, "", "",
            letter_annotations={"phred_quality": qual}
        )
            

def handle_biopython(file_str, format=None):
    in_file = my_open(file_str)
    return SeqIO.parse(in_file, format)


formats = {
    None: handle_biopython,
    "sam": handle_samfile,
    "bam": partial(handle_samfile, filemode="rb"),
}

formats.update( (key, partial(handle_biopython, format=key)) 
                for key in SeqIO._FormatToWriter.keys() )



def trim(read):
    quals = read.letter_annotations['phred_quality']
    a = count( dropwhile( low_quality, quals ) )
    b = count( dropwhile( low_quality, reversed(quals) ) )
    return read[-a:b]


def filter_reads(reads):
    longer_than_n = compose(gt(min_len), len)
    return ifilter( longer_than_n, imap(trim, reads) )


def _writer_thread(reads, filename):
    SeqIO.write(reads, filename, 'fastq')


@contextmanager
def mktempfifo(names=("a",)):
    tmpdir = tempfile.mkdtemp()
    names = map(partial(os.path.join, tmpdir), names)
    map(os.mkfifo, names)
    yield names
    map(os.remove, names)
    os.rmdir(tmpdir)
    

def bowtie2(reads_str, index_str):
    return subprocess.Popen(["bowtie2", "--very-sensitive",
                             "-x", index_str,
                             "-U", reads_str, ], stdout=subprocess.PIPE)


def align_reads(reads, index_str):
    with mktempfifo() as filenames:
        writer_thread = threading.Thread(target=_writer_thread,
                                         args=(reads, filenames[0]))
        writer_thread.daemon = True
        writer_thread.start()
        proc = bowtie2(filenames[0], index_str)
        for line in proc.stdout:
            yield line.strip().split('\t')
        writer_thread.join()
        proc.wait()
        
        
def skim_database_lengths(alignments):
    alignments = dropwhile( compose(eq("@SQ"), get(0)), alignments )
    next(alignments)
    fields = next(alignments)
    lengths = list()
    while fields[0] == "@SQ":
        name, length = fields[1:]
        lengths.append( (name.split(':')[1],
                         int(length.strip().split(':')[1])) )
        fields = next(alignments)
    return lengths, alignments


def output(counts, db_lengths, n_reads):
    for ((hit_name, count), (_, length)) in join(0, counts, 0, db_lengths):
        msg = "%s\t%.5f"
        print msg % (hit_name, (length/1000.)/(n_reads/1000000.)/2.)


def main(reads_str, index_str):
    from_format = reads_str.split(".")[-1]
    reads = formats[from_format](reads_str)
    filtered = filter_reads(reads)
    alignments = align_reads(filtered, index_str)
    db_lengths, alignments = skim_database_lengths(alignments)
    alignments = ifilter(good_mapping, alignments)
    counts = countby(get(2), alignments)
    n_reads = sum(counts.values())
    output(counts.items(), db_lengths, n_reads)


if __name__ == '__main__':
    ret = main(*sys.argv[1:])
    sys.exit(ret)
