#!/usr/bin/env python

import os
import sys
import string
import tempfile
import optparse
import operator
import threading
import subprocess
from contextlib import contextmanager
from functools import partial
from itertools import dropwhile, ifilter, imap, repeat, groupby

try:
    import bz2
    import gzip
except:
    pass

from toolz import curry, count, compose, mapcat, pluck, reduceby
from toolz.curried import get
from Bio import SeqIO, Seq, SeqRecord
import pysam

HELP = """%prog [options] -f <format> -r reference.fasta -d database_prefix [<reads_file>]

%prog - Update reference genome with new reads. New characters are chosen by the max mapping score

Available sequence formats:
""" 


opts_list = [
    optparse.make_option(
        '-f', '--format', action="store", 
        dest="from_format", type="string",
        help="The file format to convert from"),
    optparse.make_option(
        '-n', '--min_len', action='store', 
        type="int", dest='min_len', default=75,
        help="Filter input sequences by length."),
    optparse.make_option(
        '-q', '--trim_until_qual', action="store", type="int", 
        dest="min_qual", default=20,
        help="Trim sequences until this minimum quality score"),
    optparse.make_option(
        '-m', '--min_mapping_score', action="store", type="int", 
        dest="min_mapping", default=20,
        help="Trim sequences until this minimum quality score"),
    optparse.make_option(
        '-r', '--reference', action="store", type="string", 
        dest="reference", default=None,
        help="Reference this assembled genome."),
    optparse.make_option(
        '-d', '--db_prefix', action="store", type="string", 
        dest="db_prefix",
        help=("Use this database to decontaminate. "
              "Hits against this database mark the read as contaminated")),
]



lt = curry(operator.gt) #these are switched because of how curry() works
gt = curry(operator.lt) #these are switched because of how curry() works
eq = curry(operator.eq)
ne = curry(operator.ne)
first = operator.itemgetter(0)
second = operator.itemgetter(1)
third = operator.itemgetter(2)
last = operator.itemgetter(-1)

def handle_cli():
    global HELP
    HELP += str(sorted(filter(None, formats.keys())))
    parser = optparse.OptionParser(option_list=opts_list, 
                                   usage=HELP)
    return parser.parse_args()

def my_open(f, *args, **kwargs):
    if f == '-':
        return sys.stdin
    elif f.endswith(".bz2"):
        return bz2.BZ2File(f, *args, **kwargs)
    elif f.endswith("gzip") or f.endswith("gz"):
        return gzip.GzipFile(f, *args, **kwargs)
    else:
        return open(f, *args, **kwargs)


def handle_samfile(file_str, filemode="r"):
    sam_file = pysam.Samfile(file_str, filemode, 
                             check_header=False, check_sq=False)
    for read in sam_file:
        seq = Seq.Seq(read.seq)
        qual = [ ord(x)-33 for x in read.qual ]
        if read.is_reverse:
            seq = seq.reverse_complement()
        yield SeqRecord.SeqRecord(
            seq, read.qname, "", "",
            letter_annotations={"phred_quality": qual}
        )
            

def handle_biopython(file_str, format=None):
    in_file = my_open(file_str)
    return SeqIO.parse(in_file, format)


formats = {
    None: handle_biopython,
    "sam": handle_samfile,
    "bam": partial(handle_samfile, filemode="rb"),
}

formats.update( (key, partial(handle_biopython, format=key)) 
                for key in SeqIO._FormatToWriter.keys() )


def trim(is_low_quality, read):
    quals = read.letter_annotations['phred_quality']
    a = count( dropwhile( is_low_quality, quals ) )
    b = count( dropwhile( is_low_quality, reversed(quals) ) )
    return read[-a:b]


def filter_reads(reads, min_len, min_qual):
    is_low_quality = lt(min_qual)
    longer_than_n = compose(gt(min_len), len)
    trimmer = partial(trim, is_low_quality)
    trimmed = imap( trimmer, reads )
    return ifilter( longer_than_n, trimmed )


def _writer_thread(reads, filename):
    SeqIO.write(reads, filename, 'fastq')


@contextmanager
def mktempfifo(names=("a",)):
    tmpdir = tempfile.mkdtemp()
    names = map(partial(os.path.join, tmpdir), names)
    map(os.mkfifo, names)
    yield names
    map(os.remove, names)
    os.rmdir(tmpdir)
    

def bowtie2(reads_str, index_str):
    return subprocess.Popen(["bowtie2", "--very-sensitive",
                             "-x", index_str,
                             "-a",
                             "-U", reads_str, ], stdout=subprocess.PIPE)


def align_reads(reads, index_str):
    with mktempfifo() as filenames:
        writer_thread = threading.Thread(target=_writer_thread,
                                         args=(reads, filenames[0]))
        writer_thread.daemon = True
        writer_thread.start()
        proc = bowtie2(filenames[0], index_str)
        for line in proc.stdout:
            yield line.strip().split('\t')
        writer_thread.join()
        proc.wait()
        
        
def skip_headers(alignments):
    return dropwhile( compose(lt(10), len), alignments )


def output(alignments):
    for start, seq, evalue, name in pluck([3,9,4,2], alignments):
        start = int(start)
        positions = imap(str, range(start, start+len(seq)))
        packed = zip(repeat(name), positions, seq, repeat(evalue))
        for tup in packed:
            print "\t".join(tup)
        

def sequence_parse(input_files, format_str):
    handler = formats[format_str]
    return mapcat(handler, input_files)


def output_reference(reference_fname):
    sequences = sequence_parse([reference_fname], "fasta")
    output([ [None, None, s.id, "1", "1", None, None, None, None, s.seq]
             for s in sequences ])
    

def run_map(opts, input_files):
    good_mapping = compose(gt(opts.min_mapping), int, get(4)) 
    
    output_reference(opts.reference)
    reads = sequence_parse(input_files, opts.from_format)
    filtered = filter_reads(reads, opts.min_len, opts.min_qual)
    alignments = align_reads(filtered, opts.db_prefix)
    alignments = skip_headers(alignments)
    alignments = ifilter(good_mapping, alignments)
    output(alignments)

#TODO: run this function in parallel with multiprocessling.Pool
adder = lambda acc, x: acc + float(x[-1])
def winning_chars(chunk):
    group_by_pos = imap(second, groupby(chunk, key=second))
    for pos_group in group_by_pos:
        sum_evalues = reduceby(third, adder, pos_group, 0)
        yield max(sum_evalues.items(), key=second)[0]

def reduce_chunks(fields):
    for name, chunk in groupby(fields, key=first):
        name = ">{}_updated".format(name)
        yield name, winning_chars(chunk)

def run_reduce(infile):
    fields = imap(compose(string.split, string.strip), infile)
    for name, updated_reference in reduce_chunks(fields):
        sys.stdout.write(name+'\n')
        map(sys.stdout.write, updated_reference)
        sys.stdout.write("\n")


def main():  
    opts, (subcommand, input_files) = handle_cli()

    if type(input_files) is str:
        input_files = [input_files]
    if not input_files:
        input_files = ["-"]

    if subcommand == "map":
        run_map(opts, input_files)
    elif subcommand == "reduce":
        run_reduce(sys.stdin)
    else:
        raise Exception("Don't recognize that subcommand, sorry.")


if __name__ == '__main__':
    ret = main()
    sys.exit(ret)
